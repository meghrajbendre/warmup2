#include "warmup2.h"

//display cmd line usage information
static void usage() {
    fprintf(stderr,
            "usage: %s\n", "warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] "
                           "[-P P] [-n num] [-t tsfile]");
    exit(-1);
}

void cmd_line_args_init(struct cmd_line_args *obj) {
    memset(obj, 0, sizeof(struct cmd_line_args));
    obj->filename = NULL;
    obj->token_rate = 1.5;
    obj->packet_rate = 1;
    obj->service_rate = 0.35;
    obj->bucket_size = 10;
    obj->tokens_in_packet = 3;
    obj->num_packets = 3;//20;
}

struct packet_data* packet_data_init(int inter_arrival_time, int num_tokens, int service_time) {
    struct packet_data *temp_packet;
    temp_packet = (struct packet_data*)malloc(sizeof(struct packet_data));
    if(!temp_packet) {
        fprintf(stdout, "ERROR: Memory allocation for packet data failed\n");
        exit(1);
    }
    temp_packet->packet_id = -1; //unique packet identifier
    temp_packet->system_time = 0;
    temp_packet->Q1_time = 0;
    temp_packet->Q2_time = 0;

    memset(&(temp_packet->arrival_time), 0, sizeof(struct timeval));
    memset(&(temp_packet->departure_time), 0, sizeof(struct timeval));
    memset(&(temp_packet->entered_Q1_time), 0, sizeof(struct timeval));
    memset(&(temp_packet->left_Q1_time), 0, sizeof(struct timeval));
    memset(&(temp_packet->entered_Q2_time), 0, sizeof(struct timeval));
    memset(&(temp_packet->left_Q2_time), 0, sizeof(struct timeval));
    memset(&(temp_packet->arrival_time_after_sleep), 0, sizeof(struct timeval));
    
    temp_packet->inter_arrival_time = inter_arrival_time;
    temp_packet->num_tokens = num_tokens;
    temp_packet->service_time = service_time;    
    temp_packet->exact_inter_arrival_time = inter_arrival_time;
    temp_packet->exact_service_time = service_time;

    return temp_packet;
}

void print_cmd_line_args(struct cmd_line_args *obj) {
    fprintf(stdout, "Emulation Parameters:\n");
    fprintf(stdout, "\tnumber to arrive = %d\n", obj->num_packets);
    if(trace_driven)
        fprintf(stdout, "\tr = %.6g\n\tB = %d\n\ttsfile = %s\n",
                obj->token_rate, obj->bucket_size, obj->filename);
    else
        fprintf(stdout, "\tlambda = %.6g\n\tmu = %.6g\n\tr = %.6g\n\tB = %d\n\tP = %d\n",
                obj->packet_rate, obj->service_rate, obj->token_rate, obj->bucket_size,
                obj->tokens_in_packet);
}

void parse_cmdline(int argc, char *argv[], struct cmd_line_args *obj) {
    int index = 0, option = 0, cnt = 0;
    double temp = 0;
    cnt = argc;

    while((option = getopt_long_only(argc, argv, ":", longopts, &index)) != -1) {
        switch(option) {
            case 'l':
                    temp = atof(optarg);
                    if(temp > 0)
                        obj->packet_rate = temp;
                    else {
                        fprintf(stderr, "ERROR: Argument value of option -lamda "
                                        "should be a positive real number\n");
                        exit(1);
                    }     
                    break;
            case 'm':
                    temp = atof(optarg);
                    if(temp > 0)
                        obj->service_rate = temp;
                    else {
                        fprintf(stderr, "ERROR: Argument value of option -mu "
                                        "should be a positive real number\n");
                        exit(1);
                    }  
                    break;
            case 'r':
                    temp = atof(optarg);
                    if(temp > 0)
                        obj->token_rate = temp;
                    else {
                        fprintf(stderr, "ERROR: Argument value of option -r "
                                        "should be a positive real number\n");
                        exit(1);
                    }  
                    break;
            case 'b':
                    temp = atof(optarg);
                    if((temp > 0) && (temp == (int)temp))
                        obj->bucket_size = (int)temp;
                    else {
                        fprintf(stderr, "ERROR: Argument value of option -B should be "
                                        "a positive integer less than 2147483647\n");
                        exit(1);
                    }
                    break;
            case 'p':
                    temp = atof(optarg);
                    if((temp > 0) && (temp == (int)temp))
                        obj->tokens_in_packet = (int)temp;
                    else {
                        fprintf(stderr, "ERROR: Argument value of option -P should be "
                                        "a positive integer less than 2147483647\n");
                        exit(1);
                    }            
                    break;
            case 'n':
                    temp = atof(optarg);
                    if((temp > 0) && (temp == (int)temp))
                        obj->num_packets = (int)temp;
                    else {
                        fprintf(stderr, "ERROR: Argument value of option -n should be "
                                        "a positive integer less than 2147483647\n");
                        exit(1);
                    } 
                    break;
            case 't':
                    trace_driven = 1;
                    obj->filename = optarg;
                    break;                                                                                                    
            case ':':
            case '?':
            case 0:
            default:
                    fprintf(stderr, "ERROR: Malformed Input\n");
                    usage();
                    break;    
        }
    cnt -= 2;     
    }

    //short command line arguments
    if(cnt > 1) {
        fprintf(stdout, "ERROR: Malformed Input\n");
        usage();
        exit(1);
    }

    if(trace_driven) {
        struct stat buff;
        stat(obj->filename, &buff);

        //check if the given file is a directory
        if(S_ISDIR(buff.st_mode)) {
            fprintf(stderr,"ERROR: '%s' is a directory\n", obj->filename);
            exit(1);
        }

        //check if the given file exists or not
        if(!S_ISREG(buff.st_mode)) {
            fprintf(stderr,"ERROR: File '%s' does not exist\n", obj->filename);
            exit(1);
        }

        fp = fopen(obj->filename, "r");

        //check access permissions
        if(!fp) {
            fprintf(stderr, "ERROR: Access denied to the file '%s'\n", obj->filename);
            exit(1);
        }
    }
    print_cmd_line_args(obj);
}

double time_diff(struct timeval a,struct timeval b) {
    double temp;
    temp = (a.tv_sec - b.tv_sec)*1000000 + (a.tv_usec - b.tv_usec);
    //fprintf(stdout, "\n***Temp: %lf\n",temp);
    return temp;
}

struct packet_data* readLine(char LineInFile[]) {
    int i=0,j=-1,k=0;
    int inter_arrival_time=0,num_tokens=0,service_time=0;
    char temp[1025];

    while(LineInFile[i] != '\0' && LineInFile[i] != '\n') {
        if(LineInFile[i] == '\t' || LineInFile[i] == ' ') {
            i++;
            continue;
        }
        j++;

        if(j >= 3) {
            fprintf(stderr, "ERROR:\tInvalid data/formatting in file\n"
                    "\tLine: %s \n", LineInFile);
            exit(1);
        }
        
        memset(temp, 0, sizeof(temp));

        for(k=0;LineInFile[i] != '\t' && LineInFile[i] != ' ' && LineInFile[i] != '\0';k++,i++) 
            temp[k] = LineInFile[i];
        temp[k] = '\0';

        if(j == 0)
            inter_arrival_time = atoi(temp);
        if(j == 1)
            num_tokens = atoi(temp);
        if(j == 2)
            service_time = atoi(temp);
    }
    return(packet_data_init(inter_arrival_time, num_tokens, service_time));
}

void transfer_packet_to_q2(struct packet_data* pkt) {

    //Remove packet from Q1
    My402ListUnlink(&packet_queue_Q1, My402ListFirst(&packet_queue_Q1));
    gettimeofday(&(pkt->left_Q1_time), 0);
    pkt->Q1_time = (time_diff(pkt->left_Q1_time, pkt->entered_Q1_time))/1000.0;
    fprintf(stdout, "%012.3fms: p%d leaves Q1, time in Q1 = %.3fms, token bucket now has %d token\n", \
                    (time_diff(pkt->left_Q1_time, emulation_start_time))/1000.0, pkt->packet_id, \
                    pkt->Q1_time, tokens_in_bucket);
    q1_to_q2_count++;
    //Add packet to Q2
    My402ListAppend(&packet_queue_Q2,pkt);
    gettimeofday(&(pkt->entered_Q2_time), 0);
    fprintf(stdout, "%012.3fms: p%d enters Q2\n", \
                    (time_diff(pkt->entered_Q2_time, emulation_start_time))/1000.0, pkt->packet_id);
    if(!My402ListEmpty(&packet_queue_Q2)) {
        pthread_cond_broadcast(&q2_empty);
    }
}

void packet_handler(struct cmd_line_args* obj) {

    char LineInFile[1030];
    struct packet_data *temp_packet;
    double inter_arrival_time1=0, service_time1=0;
    int inter_arrival_time2=0, service_time2=0, pkt_cntr = 0;

    if(!trace_driven) {

    inter_arrival_time1 = (double)(1/obj->packet_rate);
    inter_arrival_time2 = (int)(inter_arrival_time1 * 1000);
    if(inter_arrival_time1 > 10)
        inter_arrival_time2 = 10000; //in ms

    service_time1 = (double)(1/obj->service_rate);
    service_time2 = (int)(service_time1 * 1000); //ms
    if(service_time1 > 10) 
        service_time2 = 10000;  //in ms

    } else {
        //first line in file is total num of packets to arrive
        fgets(LineInFile, sizeof(LineInFile), fp);
        if((LineInFile[0] >= '0') && (LineInFile[0] <= '9'))
                obj->num_packets = atoi(LineInFile);
        else {
                fprintf(stderr, "ERROR: Line 1 is not just a number\n");
                exit(1);
        }

    }
    while(pkt_cntr < obj->num_packets) {

        /*If all valid packets are in Q1 or out of Q2 into server), 
        there is no need of more tokens.
        So arrival thread should stop the token thread*/

        if((packets_processed+packets_dropped) == obj->num_packets) { //all packets are takein into consideration
            if((q1_to_q2_count == packets_processed) && (My402ListEmpty(&packet_queue_Q1)))
                pthread_cancel(token_thread);
        }

        if(!trace_driven) {

            temp_packet = packet_data_init(0, 0, 0);
            temp_packet->packet_id = packets_processed + packets_dropped + 1;
            temp_packet->num_tokens = obj->tokens_in_packet;
            temp_packet->inter_arrival_time = inter_arrival_time2; //ms
            temp_packet->service_time = service_time2; //ms

        } else {

            //read line from file
            fgets(LineInFile, sizeof(LineInFile), fp);
            if(strlen(LineInFile) > 1024) {
                fprintf(stderr, "ERROR: Line has more than 1024 characters\n");
                exit(1);
            }
            temp_packet = readLine(LineInFile);
            temp_packet->packet_id = packets_processed + packets_dropped + 1;
            //set arrival time of the packet
        }

        gettimeofday(&temp_packet->arrival_time, 0);
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0);	
	    usleep(temp_packet->inter_arrival_time*1000);
	    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, 0);
        
        //get mutex lock
        pthread_mutex_lock(&mutex);        
        gettimeofday(&(temp_packet->arrival_time_after_sleep), 0);
        temp_packet->exact_inter_arrival_time = ((time_diff(temp_packet->arrival_time_after_sleep, temp_packet->arrival_time))/1000.0);

        //print for packet not dropped
        if(temp_packet->num_tokens <= obj->bucket_size)
            fprintf(stdout, "%012.3fms: p%d arrives, needs %d tokens, inter-arrival time = %.3fms\n", \
                            (time_diff(temp_packet->arrival_time_after_sleep, emulation_start_time))/1000.0, \
                            temp_packet->packet_id, (int)temp_packet->num_tokens, \
                            temp_packet->exact_inter_arrival_time);
        else {//print for packet dropped
            fprintf(stdout, "%012.3fms: p%d arrives, needs %d tokens, inter-arrival time = %.3fms, dropped\n", \
                            (time_diff(temp_packet->arrival_time_after_sleep, emulation_start_time))/1000.0, \
                            temp_packet->packet_id, (int)temp_packet->num_tokens, \
                            temp_packet->exact_inter_arrival_time);
           total_packet_interval_time += temp_packet->exact_inter_arrival_time; 
            free(temp_packet);
            packets_dropped++;
            pkt_cntr++;
            pthread_mutex_unlock(&mutex);
            continue;
        }
        //enqueue Q1
        My402ListAppend(&packet_queue_Q1, temp_packet);
        gettimeofday(&(temp_packet->entered_Q1_time), 0);
        fprintf(stdout, "%012.3fms: p%d enters Q1\n",
                        (time_diff(temp_packet->entered_Q1_time, emulation_start_time))/1000.0, \
                        temp_packet->packet_id);
        
        packets_processed++;
        total_packet_interval_time += temp_packet->exact_inter_arrival_time; 
        //move packet from Q1 to Q2
        My402ListElem *elem = My402ListFirst(&packet_queue_Q1);
        if(elem == NULL) {
            pthread_mutex_unlock(&mutex);
            break;
        }
        struct packet_data *pkt = (struct packet_data*)elem->obj;        
        if((int)pkt->num_tokens <= tokens_in_bucket) {
            tokens_in_bucket -= (int)pkt->num_tokens;
            transfer_packet_to_q2(pkt);
        }
        pthread_mutex_unlock(&mutex);

        if((packets_processed+packets_dropped) == obj->num_packets) { //all packets are takein into consideration
            if((q1_to_q2_count == packets_processed) && (My402ListEmpty(&packet_queue_Q1)))
                pthread_cancel(token_thread);
        }

        pkt_cntr++;
    }

    if((packets_processed+packets_dropped) == obj->num_packets) { //all packets are takein into consideration
        if((q1_to_q2_count == packets_processed) && (My402ListEmpty(&packet_queue_Q1)))
            pthread_cancel(token_thread);
    }

    if((packets_processed+packets_dropped) == obj->num_packets) { //all packets are takein into consideration
        if((packets_processed == packets_completed) && (My402ListEmpty(&packet_queue_Q2)))
            stop_servers = 1;
            pthread_cond_broadcast(&q2_empty);
    }

    if(trace_driven)
        fclose(fp);
    //pthread_exit(NULL);
}

void token_bucket_handler(struct cmd_line_args* obj) {
    double temp_token_rate = 0,current_time_ms=0;
    int token_inter_arrival_time = 0;
    struct timeval current_time;

    temp_token_rate = (1.0/obj->token_rate);
    if(temp_token_rate > 10.0)
        temp_token_rate = 10.0;    
    token_inter_arrival_time = (int)(temp_token_rate*1000.0); //ms

    while(1) {
    
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0); 
        usleep(token_inter_arrival_time*1000);
        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, 0);
        
        pthread_mutex_lock(&mutex); //get mutex lock
        gettimeofday(&current_time, 0);
        current_time_ms = (time_diff(current_time, emulation_start_time))/1000.0;

        if(tokens_in_bucket >= obj->bucket_size) {
            tokens_dropped++;
            fprintf(stdout, "%012.3fms: token t%d arrives, dropped\n",current_time_ms, tokens_processed+tokens_dropped);
            pthread_mutex_unlock(&mutex);
            continue;
        }
        //increment tokens in the bucket
        tokens_in_bucket++;
        tokens_processed++;
        fprintf(stdout, "%012.3fms: token t%d arrives, token bucket now has %d token%c\n",
                        current_time_ms, tokens_processed+tokens_dropped, 
                        tokens_in_bucket,(tokens_in_bucket > 1?'s':' '));
        //move packet from Q1 to Q2
        My402ListElem *elem = My402ListFirst(&packet_queue_Q1);
        if(elem == NULL) {
            pthread_mutex_unlock(&mutex);
            continue;
        }
        struct packet_data *pkt = (struct packet_data*)elem->obj;        
        if((int)pkt->num_tokens <= tokens_in_bucket) {
            tokens_in_bucket -= (int)pkt->num_tokens;
            transfer_packet_to_q2(pkt);
            /*if(!My402ListEmpty(&packet_queue_Q2)) {
                pthread_cond_broadcast(&cv);
            }*/
        }
        //unlock mutex
        pthread_mutex_unlock(&mutex);
         //all packets are takein into consideration
        if(((q1_to_q2_count+packets_dropped) == obj->num_packets) && (My402ListEmpty(&packet_queue_Q1))) {
                break;
        }
    }
    if(My402ListEmpty(&packet_queue_Q2)) {
		stop_servers = 1;
		pthread_cond_broadcast(&q2_empty);
	}
    pthread_exit((void*)0);
}

void server_handler1(struct cmd_line_args* obj) {

    int pkts_processed_by_server=0;
    struct timeval current_time;
    double current_time_ms=0;
    while((pkts_processed_by_server < obj->num_packets) && (stop_servers != 1)) {
        pthread_mutex_lock(&mutex);
        //wait for packets to be queued in Q2
        while(My402ListEmpty(&packet_queue_Q2) && !stop_servers){
        	pthread_cond_wait(&q2_empty, &mutex);
        }
		if(stop_servers) {
			pthread_mutex_unlock(&mutex);
			break;
		}

        My402ListElem *elem = My402ListFirst(&packet_queue_Q2);
        if(elem == NULL) {
            pthread_mutex_unlock(&mutex);
            break;
        }
        struct packet_data *pkt = (struct packet_data*)elem->obj;
        My402ListUnlink(&packet_queue_Q2, elem);
 
        pthread_mutex_unlock(&mutex);
        gettimeofday(&pkt->left_Q2_time, 0);
        pkt->Q2_time = (time_diff(pkt->left_Q2_time, pkt->entered_Q2_time))/1000.0;
        fprintf(stdout, "%012.3fms: p%d leaves Q2, time in Q2 = %.3fms\n", \
                        (time_diff(pkt->left_Q2_time, emulation_start_time))/1000.0, pkt->packet_id, \
                        pkt->Q2_time);

        gettimeofday(&current_time, 0);
        current_time_ms = (time_diff(current_time, emulation_start_time))/1000.0;        
        fprintf(stdout, "%012.3fms: p%d begin service at S1, requesting %dms of service\n", \
                        current_time_ms, pkt->packet_id, pkt->service_time);

        usleep(pkt->service_time*1000);
 
        gettimeofday(&pkt->departure_time, 0);
        pkt->exact_service_time = (time_diff(pkt->departure_time, current_time))/1000.0;
        pkt->system_time = (time_diff(pkt->departure_time, pkt->arrival_time_after_sleep))/1000.0;
        fprintf(stdout, "%012.3fms: p%d departs from S1, service time = %.3fms, time in system = %.3fms\n",
                    (time_diff(pkt->departure_time,emulation_start_time))/1000.0, pkt->packet_id, 
                    pkt->exact_service_time, pkt->system_time);
        packets_completed++;

        total_Q1_time += pkt->Q1_time;
        total_Q2_time += pkt->Q2_time;
        total_S1_time += pkt->exact_service_time;
        total_system_time += pkt->system_time;
        squared_system_time += (pkt->system_time*pkt->system_time);
        total_packet_service_time += pkt->exact_service_time;

    
        if(My402ListEmpty(&packet_queue_Q2) && ((q1_to_q2_count+packets_dropped)==obj->num_packets)) {
    		stop_servers = 1;
	    	pthread_cond_broadcast(&q2_empty);
	    }
        pkts_processed_by_server++;
    }
    pthread_exit((void*)0);
} 

void server_handler2(struct cmd_line_args* obj) {

    int pkts_processed_by_server=0;
    struct timeval current_time;
    double current_time_ms=0;
    while((pkts_processed_by_server < obj->num_packets) && (stop_servers != 1)) {
        pthread_mutex_lock(&mutex);
        //wait for packets to be queued in Q2
        while(My402ListEmpty(&packet_queue_Q2) && !stop_servers){
        	pthread_cond_wait(&q2_empty, &mutex);
        }
		if(stop_servers) {
			pthread_mutex_unlock(&mutex);
			break;
		}

        My402ListElem *elem = My402ListFirst(&packet_queue_Q2);
        if(elem == NULL) {
            pthread_mutex_unlock(&mutex);
            break;
        }
        struct packet_data *pkt = (struct packet_data*)elem->obj;
        My402ListUnlink(&packet_queue_Q2, elem);

        pthread_mutex_unlock(&mutex);
        gettimeofday(&pkt->left_Q2_time, 0);
        pkt->Q2_time = (time_diff(pkt->left_Q2_time, pkt->entered_Q2_time))/1000.0;
        fprintf(stdout, "%012.3fms: p%d leaves Q2, time in Q2 = %.3fms\n", \
                        (time_diff(pkt->left_Q2_time, emulation_start_time))/1000.0, pkt->packet_id, \
                        pkt->Q2_time);

        gettimeofday(&current_time, 0);
        current_time_ms = (time_diff(current_time, emulation_start_time))/1000.0;        
        fprintf(stdout, "%012.3fms: p%d begin service at S2, requesting %dms of service\n", \
                        current_time_ms, pkt->packet_id, pkt->service_time);

        usleep(pkt->service_time*1000);

        gettimeofday(&pkt->departure_time, 0);
        pkt->exact_service_time = (time_diff(pkt->departure_time, current_time))/1000.0;
        pkt->system_time = (time_diff(pkt->departure_time, pkt->arrival_time_after_sleep))/1000.0;
        fprintf(stdout, "%012.3fms: p%d departs from S2, service time = %.3fms, time in system = %.3fms\n",
                    (time_diff(pkt->departure_time,emulation_start_time))/1000.0, pkt->packet_id, 
                    pkt->exact_service_time, pkt->system_time);
        packets_completed++;

        total_Q1_time += pkt->Q1_time;
        total_Q2_time += pkt->Q2_time;
        total_S2_time += pkt->exact_service_time;
        total_system_time += pkt->system_time;
        squared_system_time += (pkt->system_time*pkt->system_time);
        total_packet_service_time += pkt->exact_service_time;
  
    
        if(My402ListEmpty(&packet_queue_Q2) && ((q1_to_q2_count+packets_dropped)==obj->num_packets)) {
    		stop_servers = 1;
	    	pthread_cond_broadcast(&q2_empty);
	    }
        pkts_processed_by_server++;
    }
    pthread_exit((void*)0);
} 

void calculate_statistics(struct cmd_line_args *obj) {
    double time_spent_in_emulation = (time_diff(emulation_end_time, emulation_start_time))/1000.0; 
    double avg_packet_service_time = (total_packet_service_time/packets_completed);
    double avg_system_time = ( total_system_time / packets_completed );
    double token_drop_probability = (double)tokens_dropped / (tokens_dropped + tokens_processed);
    double packet_drop_probability = ((double) packets_dropped)/(packets_processed + packets_dropped) ;
    double avg_squared_system_time = ( squared_system_time / packets_completed);
    double std_dev = sqrt(avg_squared_system_time - (avg_system_time*avg_system_time));

	fprintf(stdout,"Statistics:\n\n");

	if(packets_dropped+packets_processed)
		fprintf(stdout,"\taverage packet inter-arrival time = %.6g sec\n",
                        ((total_packet_interval_time/(packets_processed + packets_dropped))/1000));
    else
        fprintf(stdout,"\taverage packet inter-arrival time = N/A, no packets were received\n");

	if(packets_completed)
		fprintf(stdout,"\taverage packet service time = %.6g sec\n\n",(avg_packet_service_time/1000));
    else
        fprintf(stdout,"\taverage packet service time = N/A, no packets were served by the server\n\n");
	if(time_spent_in_emulation) {
		fprintf(stdout,"\taverage number of packets in Q1 = %.6g packets\n",
                        (total_Q1_time / time_spent_in_emulation));
		fprintf(stdout,"\taverage number of packets in Q2 = %.6g packets\n",
                        (total_Q2_time / time_spent_in_emulation));
		fprintf(stdout,"\taverage number of packets at S1 = %.6g packets\n",
                        (total_S1_time / time_spent_in_emulation));
		fprintf(stdout,"\taverage number of packets at S2 = %.6g packets\n\n",
                        (total_S2_time / time_spent_in_emulation));
    }
	if(packets_completed) {
		fprintf(stdout,"\taverage time a packet spent in system = %.6g sec\n",(avg_system_time/1000));
		fprintf(stdout,"\tstandard deviation for time spent in system = %.6g sec\n\n",(std_dev/1000));
    } else {
        fprintf(stdout,"\taverage time a packet spent in system = N/A, no packets were served by the server\n");
		fprintf(stdout,"\tstandard deviation for time spent in system = N/A, no packets were served by the server\n\n");
    }
	if(tokens_processed+tokens_dropped)
		fprintf(stdout,"\ttoken drop probability = %.6g\n",token_drop_probability);
    else
        fprintf(stdout,"\ttoken drop probability = N/A, tokens were not generated\n");
	if(packets_processed+packets_dropped)
		fprintf(stdout,"\tpacket drop probability = %.6g\n",packet_drop_probability);
    else
        fprintf(stdout,"\tpacket drop probability = N/A, packets were not generated\n");

}

void signal_handler(struct cmd_line_args * obj) {
    struct timeval current_time;
    double current;
	while(1) {
        int sig_no;
        sigwait(&signal_handler_set, &sig_no);
    
        pthread_mutex_lock(&mutex);
        gettimeofday(&current_time, 0);
        current = (time_diff(current_time, emulation_start_time)/1000.0);
        fprintf(stdout, "\n%012.3fms: SIGINT caught, no new packets or tokens will be allowed\n",current);    
        stop_servers = 1;

        pthread_cond_broadcast(&q2_empty);
        pthread_cancel(packet_thread);
        pthread_cancel(token_thread);

        if(!My402ListEmpty(&packet_queue_Q1)) {
            My402ListElem *temp = NULL;
            temp = My402ListFirst(&packet_queue_Q1);
            while(temp != NULL){
                struct packet_data *pkt = (struct packet_data*)temp->obj;
                gettimeofday(&current_time, 0);
                current = (time_diff(current_time, emulation_start_time)/1000.0);
                fprintf(stdout, "%012.3fms: p%d removed from Q1\n", current, pkt->packet_id);
                My402ListUnlink(&packet_queue_Q1, temp);
                temp = My402ListNext(&packet_queue_Q1, temp);		
            }
        }

        if(!My402ListEmpty(&packet_queue_Q2)) {
            My402ListElem *temp = NULL;
            temp = My402ListFirst(&packet_queue_Q2);
            while(temp != NULL){
                struct packet_data *pkt = (struct packet_data*)temp->obj;
                gettimeofday(&current_time, 0);
                current = (time_diff(current_time, emulation_start_time)/1000.0);
                fprintf(stdout, "%012.3fms: p%d removed from Q2\n", current, pkt->packet_id);
                My402ListUnlink(&packet_queue_Q2, temp);
                temp = My402ListNext(&packet_queue_Q2, temp);		
            }
        }
        pthread_mutex_unlock(&mutex);
        pthread_exit((void*)0);
    }
}

int main(int argc, char *argv[]) {
    struct cmd_line_args obj;
    cmd_line_args_init(&obj);
    parse_cmdline(argc, argv, &obj);

    sigemptyset(&signal_handler_set);
    sigaddset(&signal_handler_set, SIGINT);
    pthread_sigmask(SIG_BLOCK, &signal_handler_set, NULL);

    //Initializations
    if(!My402ListInit(&token_bucket)){
        fprintf(stderr,"ERROR: Intialization of token buket failed\n");
        exit(1);
    }            

    if(!My402ListInit(&packet_queue_Q1)){
        fprintf(stderr,"ERROR: Intialization of Q1 failed\n");
        exit(1);
    }

    if(!My402ListInit(&packet_queue_Q2)){
        fprintf(stderr,"ERROR: Intialization of Q2 failed\n");
        exit(1);
    }

    gettimeofday(&emulation_start_time, 0);
    fprintf(stdout, "\n%012.3fms: emulation begins\n",(double)0);

    //Thread handlers
    if(pthread_create(&packet_thread, 0, (void*)packet_handler, &obj) != 0) {   //trace driven
        fprintf(stderr, "ERROR: Thread creation for packet failed\n");
        exit(1);
    }
    if(pthread_create(&token_thread, 0, (void*)token_bucket_handler, &obj) != 0) {
            fprintf(stderr, "ERROR: Thread creation for token failed\n");
            exit(1);
    }
    if(pthread_create(&server_thread1, 0, (void*)server_handler1, &obj) != 0) {
            fprintf(stderr, "ERROR: Thread creation for server failed\n");
            exit(1);
    }
    if(pthread_create(&server_thread2, 0, (void*)server_handler2, &obj) != 0) {
            fprintf(stderr, "ERROR: Thread creation for server failed\n");
            exit(1);
    }
    if(pthread_create(&signal_thread, 0, (void*)signal_handler, &obj) != 0) {
            fprintf(stderr, "ERROR: Thread creation for signal failed\n");
            exit(1);
    }

    pthread_join(packet_thread, 0);
    pthread_join(token_thread, 0);
    pthread_join(server_thread1, 0);
    pthread_join(server_thread2, 0);
   // pthread_join(signal_thread, 0);

    gettimeofday(&emulation_end_time, 0);
    fprintf(stdout, "%012.3fms: emulation ends\n", 
                    time_diff(emulation_end_time,emulation_start_time)/1000.0);
    calculate_statistics(&obj);
    My402ListUnlinkAll(&token_bucket);
    My402ListUnlinkAll(&packet_queue_Q1);
    My402ListUnlinkAll(&packet_queue_Q2);
    
    return 0;
}