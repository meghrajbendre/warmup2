#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <math.h>
#include "my402list.h"

/*STRUCTURE DECLARATIONS*/
//following structure defines options for cmd line arguments
static const struct option longopts[] = {
    {"lambda",  required_argument,  NULL,   'l' },
    {"mu",      required_argument,  NULL,   'm'},
    {"r",       required_argument,  NULL,   'r'},
    {"B",       required_argument,  NULL,   'b'},
    {"P",       required_argument,  NULL,   'p'},
    {"n",       required_argument,  NULL,   'n'},
    {"t",       required_argument,  NULL,   't'},
    {0,0,0,0}
};

//structure for cmd line arguments
struct cmd_line_args {
char* filename;         //file address
double token_rate;      //token arrival rate: r
double packet_rate;     //packet arrival rate: lamda
double service_rate;    //service time: mu
int bucket_size;        //bucket size: B
int tokens_in_packet;   //no. of tokens required for transmission of a packet: P
int num_packets;        //no. of packets to arrive: n
};

//structure for data related to a packet
struct packet_data {
    int packet_id;
    int inter_arrival_time, num_tokens ,service_time;    //read from file
    struct timeval arrival_time, departure_time, entered_Q1_time, \
                   left_Q1_time, entered_Q2_time, left_Q2_time, 
                   arrival_time_after_sleep;
    double exact_inter_arrival_time, exact_service_time, system_time, \
           Q1_time, Q2_time;
};
//time-keeping for the emulation
struct timeval emulation_start_time, emulation_end_time;

/*VARIABLE DECLARATIONS*/
int trace_driven=0; //flag to keep track of emulation method method 
int tokens_in_bucket=0;   //number of tokens in the bucket at a given instance
int tokens_processed=0;   //used tokens
int tokens_dropped=0;     //unaccepted tokens
int packets_processed=0;   //packets that made it to Q1
int packets_dropped=0;     //dropped before adding to Q1
int q1_to_q2_count=0;      //packets that were moved to Q2 from Q1
int packets_completed=0;   //successfully exited server after the service time
double total_packet_interval_time=0;
double total_packet_service_time=0;
int stop_servers=0;       //flage to tell  servers to stop servicing the packets
int token_inter_arrival_time=0;
double total_Q1_time=0, total_Q2_time=0;
double total_S1_time=0, total_S2_time=0;
double total_system_time=0, squared_system_time=0;

FILE *fp = NULL; //file handler

My402List packet_queue_Q1;
My402List packet_queue_Q2;
My402List token_bucket;

pthread_t packet_thread, token_thread, server_thread1, server_thread2, signal_thread;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
pthread_cond_t q2_empty = PTHREAD_COND_INITIALIZER;

sigset_t signal_handler_set;
struct sigaction act;
int sig_notifier=0;

/*FUNCTION DECLARATIONS*/
//usage description of the cmd line arguments
static void usage();
//Intialize cmd_line_args structure
void cmd_line_args_init(struct cmd_line_args*);
//check the given cmd args for errors
void parse_cmdline(int, char* [], struct cmd_line_args*);
//print the values of cmd_line_args
void print_cmd_line_args(struct cmd_line_args*);
//handles trace driven mode
void packet_handler(struct cmd_line_args*);
//handles the thread deposition in the bucket
void token_bucket_handler(struct cmd_line_args*);
//services packets in the received by the server
void server_handler(struct cmd_line_args*);
//constructor for packet data object
struct packet_data* packet_data_init();
//reads a line from the given file
struct packet_data* readLine(char[]);
//returns time difference between two timeval stuctures in microseconds
double time_diff(struct timeval, struct timeval);
//moves a packet from Q1 into Q2 and signals server to accept it
void transfer_packet_to_q2(struct packet_data*);
//ctrl+C handling code
void signal_handler(struct cmd_line_args * obj);
