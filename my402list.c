/******************************************************************************/
/* Important CSCI 402 usage information:                                      */
/*                                                                            */
/* This fils is part of CSCI 402 programming assignments at USC.              */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

/*
 * Author:      Meghraj Bendre (bendre@usc.edu)
 *
 * @(#)$Id: my402list.c,v 1.1 2018/05/22 $
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>

#include "cs402.h"
#include "my402list.h"

//Returns the number of elements in the list
int  My402ListLength(My402List* list){
	return (list->num_members);
}

//Returns TRUE if the list is empty. Returns FALSE otherwise
int  My402ListEmpty(My402List* list){
	if(list->num_members == 0)
		return 1;
	return 0;
}

//If list is empty, just add obj to the list. Otherwise, add obj after Last().
//This function returns TRUE if the operation is performed successfully and returns FALSE otherwise
int  My402ListAppend(My402List* list, void* obj){
	My402ListElem* new_elem = NULL;

	if(My402ListEmpty(list))
	{
		new_elem = (My402ListElem*)malloc(sizeof(My402ListElem));
		if(!new_elem)
		{
			fprintf(stderr, "ERROR: Could not allocate memory");
			return 0;
		}
		new_elem->obj = obj;
		list->anchor.next = new_elem;
		new_elem->prev = &(list->anchor);
		new_elem->next = &(list->anchor);
		list->anchor.prev = new_elem;
		list->num_members = 1;
	}
	else {
                new_elem = (My402ListElem*)malloc(sizeof(My402ListElem));
                if(!new_elem)
                {
                        fprintf(stderr, "ERROR: Could not allocate memory");
                        return 0;
                }
		new_elem->obj = obj;
		new_elem->prev = list->anchor.prev;
		list->anchor.prev->next = new_elem;
		list->anchor.prev = new_elem;
		new_elem->next = &(list->anchor);
		list->num_members++;		
	}
	return 1;
}

//If list is empty, just add obj to the list. Otherwise, add obj before First().
//This function returns TRUE if the operation is performed successfully and returns FALSE otherwise
int  My402ListPrepend(My402List* list, void* obj){
        My402ListElem* new_elem = NULL;

        if(My402ListEmpty(list))
        {
                new_elem = (My402ListElem*)malloc(sizeof(My402ListElem));
                if(!new_elem)
                {
                        fprintf(stderr, "ERROR: Could not allocate memory");
                        return 0;
                }
                new_elem->obj = obj;
                list->anchor.next = new_elem;
                new_elem->prev = &(list->anchor);
                new_elem->next = &(list->anchor);
                list->anchor.prev = new_elem;
                list->num_members = 1;
        }
        else {
                new_elem = (My402ListElem*)malloc(sizeof(My402ListElem));
                if(!new_elem)
                {
                        fprintf(stderr, "ERROR: Could not allocate memory");
                        return 0;
                }
                new_elem->obj = obj;
                new_elem->prev = &(list->anchor);
		new_elem->next = list->anchor.next;
                list->anchor.next->prev = new_elem;
                list->anchor.next = new_elem;
                list->num_members++;
        }
        return 1;
}

//Unlink and delete elem from the list. Do not delete the object pointed
// to by elem and do not check if elem is on the list
void My402ListUnlink(My402List* list, My402ListElem* elem){
	elem->prev->next = elem->next;
	elem->next->prev = elem->prev;



	list->num_members--;
	free(elem);
}

//Unlink and delete all elements from the list and make the list empty
//do not delete the objects pointed to by the list elements
void My402ListUnlinkAll(My402List* list){
	My402ListElem *temp = NULL;
	temp = My402ListFirst(list);
	while(temp != NULL){
		My402ListUnlink(list, temp);
		temp = My402ListNext(list, temp);		
	}
}

/*Insert obj between elem and elem->next. If elem is NULL, then this is
 the same as Append(). This function returns TRUE if the operation is
performed successfully and returns FALSE otherwise. Do not check if 
elem is on the list*/
int  My402ListInsertAfter(My402List* list, void* obj, My402ListElem* elem){
	if(elem == NULL)
		My402ListAppend(list,obj);
	else{
		My402ListElem* new_elem = NULL;
                new_elem = (My402ListElem*)malloc(sizeof(My402ListElem));
                if(!new_elem)
                {
                        fprintf(stderr, "ERROR: Could not allocate memory");
                        return 0;
                }
                new_elem->obj = obj;
		new_elem->next = elem->next;
		new_elem->prev = elem;
		elem->next->prev = new_elem;
		elem->next = new_elem;

                list->num_members++;		
	}
	return 1;	
}

/*Insert obj between elem and elem->prev. If elem is NULL, then this is the
same as Prepend(). This function returns TRUE if the operation is performed
successfully and returns FALSE otherwise. Please do not check if elem 
is on the list*/
int  My402ListInsertBefore(My402List* list, void* obj, My402ListElem* elem){
	if(elem == NULL){
		My402ListPrepend(list,obj);
	}
	else{
		My402ListElem* new_elem = NULL;
                new_elem = (My402ListElem*)malloc(sizeof(My402ListElem));
                if(!new_elem)
                {
                        fprintf(stderr, "ERROR: Could not allocate memory");
                        return 0;
                }
                new_elem->obj = obj;
		new_elem->next = elem;
		new_elem->prev = elem->prev;
		elem->prev->next = new_elem;
		elem->prev = new_elem;

                list->num_members++;		
	}
	return 1;
}

//Returns the first list element or NULL if the list is empty
My402ListElem *My402ListFirst(My402List* list){
	//if(list->anchor.next)
	//	return list->anchor.next;
	if(My402ListEmpty(list))
		return NULL;
	return list->anchor.next;
}

//Returns the last list element or NULL if the list is empty
My402ListElem *My402ListLast(My402List* list){
        if(My402ListEmpty(list))
                return NULL;
        return list->anchor.prev;
}

//Returns elem->next or NULL if elem is the last item on the list
//Do not check if elem is on the list.
My402ListElem *My402ListNext(My402List* list, My402ListElem* elem){
	if(elem->next == &(list->anchor))
		return NULL;
	return elem->next;
}

//Returns elem->prev or NULL if elem is the first item on the list
//Do not check if elem is on the list
My402ListElem *My402ListPrev(My402List* list, My402ListElem* elem){
	if(elem->prev == &(list->anchor))
		return NULL;
	return elem->prev;
}

//Returns the list element elem such that elem->obj == obj
//Returns NULL if no such element can be found
My402ListElem *My402ListFind(My402List* list, void* obj){
	//if(My402ListEmpty(list))
	//	return NULL;

	My402ListElem *temp = NULL;
	temp = My402ListFirst(list);
	while(temp != NULL){
		if(temp->obj == obj)
			return temp;
		temp = My402ListNext(list, temp);
	}
	return NULL;
}

/*Initialize the list into an empty list
Returns TRUE if all is well and returns FALSE if there 
is an error initializing the list.*/
int My402ListInit(My402List* list){

	list->anchor.next = NULL;
	list->anchor.prev = NULL;
	list->anchor.obj = NULL;
	list->num_members = 0;
	return TRUE;
}

